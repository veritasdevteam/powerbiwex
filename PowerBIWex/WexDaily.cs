﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PowerBIWex
{
    class WexDaily
    {

        public static void DailyWex()
        {
            ClearTable();
            DBO.clsDBO clR = new DBO.clsDBO();
            long lDay = 0;
            double cnt = 0;
            string dDate = "";
            string SQL = "";
            DateTime A1 = DateTime.Today;
            DateTime A2 = new DateTime(2020, 3, 4);
            System.TimeSpan diffresult = A1 - A2;
            lDay = diffresult.Days;

            do {
                dDate = A2.AddDays(cnt).ToString();
                SQL = "insert into veritaspowerbi.dbo.claimwexdaily " +
                    "(paiddate, dailytype, Amt) " +
                    "select '" + dDate + "', " +
                    "'All', " +
                    "avg(paidamt) " +
                    "from (" +
                    "select  sum(cd.paidamt) as PaidAmt " +
                    "from Claimdetail cd " +
                    "inner join ClaimPaymentLink cpl on cd.ClaimDetailID = cpl.ClaimDetailID " +
                    "inner join ClaimPayment cp on cp.ClaimPaymentID = cpl.ClaimPaymentID " +
                    "where cd.datepaid = '" + dDate + "' " +
                    "and not cp.DateTransmitted is null " +
                    "and not cp.wexcode is null " +
                    "and cd.ClaimDetailStatus = 'Paid' " +
                    "and paidamt <> 0 " +
                    "group by ClaimID) aa";
                clR.RunSQL(SQL, Program.sCON);

                SQL = "insert into veritaspowerbi.dbo.claimwexdaily " +
                    "(paiddate, dailytype, Amt) " +
                    "select '" + dDate + "', " +
                    "'All No Ancillary', " +
                    "avg(paidamt) " +
                    "from (" +
                    "select  sum(cd.paidamt) as PaidAmt " +
                    "from Claimdetail cd " +
                    "inner join ClaimPaymentLink cpl on cd.ClaimDetailID = cpl.ClaimDetailID " +
                    "inner join ClaimPayment cp on cp.ClaimPaymentID = cpl.ClaimPaymentID " +
                    "inner join claim cl on cl.claimid = cd.claimid " +
                    "inner join contract c on c.contractid = cl.contractid " + 
                    "where cd.datepaid = '" + dDate + "' " +
                    "and not cp.DateTransmitted is null " +
                    "and cd.ClaimDetailStatus = 'Paid' " +
                    "and paidamt <> 0 " +
                    "and not c.programid in (67,103,105)" +
                    "group by cd.ClaimID) aa";
                clR.RunSQL(SQL, Program.sCON);

                SQL = "insert into veritaspowerbi.dbo.claimwexdaily " +
                    "(paiddate, dailytype, Amt) " +
                    "select '" + dDate + "', " +
                    "'All No AN', " +
                    "case when avg(paidamt) is null then 0 else avg(paidamt) end " +
                    "from (" +
                    "select  sum(cd.paidamt) as PaidAmt " +
                    "from Claimdetail cd " +
                    "inner join ClaimPaymentLink cpl on cd.ClaimDetailID = cpl.ClaimDetailID " +
                    "inner join ClaimPayment cp on cp.ClaimPaymentID = cpl.ClaimPaymentID " +
                    "inner join claim cl on cl.claimid = cd.claimid " +
                    "inner join contract c on c.contractid = cl.contractid " +
                    "where cd.datepaid = '" + dDate + "' " +
                    "and not cp.DateTransmitted is null and cd.ClaimDetailStatus = 'Paid' " +
                    "and paidamt <> 0 " +
                    "and an = 0 " +
                    "group by cd.ClaimID) aa";
                clR.RunSQL(SQL, Program.sCON);

                SQL = "insert into veritaspowerbi.dbo.claimwexdaily " +
                    "(paiddate, dailytype, Amt) " +
                    "select '" + dDate + "', " +
                    "'All no Ancillary/AN', " +
                    "case when avg(paidamt) is null then 0 else avg(paidamt) end " +
                    "from (" +
                    "select  sum(cd.paidamt) as PaidAmt " +
                    "from Claimdetail cd " +
                    "inner join ClaimPaymentLink cpl on cd.ClaimDetailID = cpl.ClaimDetailID " +
                    "inner join ClaimPayment cp on cp.ClaimPaymentID = cpl.ClaimPaymentID " +
                    "inner join claim cl on cl.claimid = cd.claimid " +
                    "inner join contract c on c.contractid = cl.contractid " +
                    "where cd.datepaid = '" + dDate + "' " +
                    "and not cp.DateTransmitted is null and cd.ClaimDetailStatus = 'Paid' " +
                    "and paidamt <> 0 " +
                    "and not c.programid in (67,103,105)" +
                    "and an = 0 " +

                    "group by cd.ClaimID) aa";
                clR.RunSQL(SQL, Program.sCON);

                cnt++;
            } while (cnt < lDay);



        }

        static void ClearTable()
        {
            DBO.clsDBO clR = new DBO.clsDBO();
            string SQL = "";
            SQL = "truncate table veritaspowerbi.dbo.claimwexdaily ";
            clR.RunSQL(SQL, Program.sCON);
        }
    }
}
